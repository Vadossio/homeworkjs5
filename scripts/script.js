"use strict";
// 1. Метод об'єкту - це деякі властивості, які мыстять визначення функцый.
// 2. Тип object – особливий. Всі інші типи називаються «примітивними», тому що їх значеннями можуть 
//    бути лише прості значення (чи то рядок, чи число, чи щось ще). У об'єктах зберігають колекції даних чи складніші структури.
// 3. Посилальний тип даних - типи, які можуть містити дуже великі обсяги дуже різнорідних даних.
//   Вони містять посилання на місце в памяті, де знаходяться реальні дані.

function createNewUser() {
    this.firstName = prompt("Enter your firstname");
    this.lastName = prompt("Enter your lastname");
    this.getLogin = function() {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    }
}
let newUser = new createNewUser();
console.log(newUser.getLogin());